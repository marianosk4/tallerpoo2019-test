package taller_1;

import java.time.LocalDate;

/**
 *
 * @author mariano
 */
public class Inscripcion {

    private LocalDate fecha;
    private String periodo;
    private Alumno alumno;
    private Asignatura asignatura;
    private TipoRegularidad regularidad;


    public Inscripcion(LocalDate fecha, String periodo, Alumno alumno, Asignatura asignatura) {
        this.asignatura = asignatura;
        this.fecha = fecha;
        this.periodo = periodo;
        this.alumno = alumno;
        this.asignatura.getListaAlumnos().add(alumno);
		this.alumno.agregarAsignatura(asignatura);
        this.regularidad = TipoRegularidad.REGULAR;

    }
	
	

    public TipoRegularidad getRegularidad() {
        return regularidad;
    }

    public void setRegularidad(TipoRegularidad regularidad) {
        this.regularidad = regularidad;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "Inscripcion{" + "fecha=" + fecha + ", periodo=" + periodo + ", alumno=" + alumno + ", asignatura=" + asignatura + '}';
    }

    public String toCSV() {
        return fecha + "," + periodo + "," + alumno.getDni() + "," + asignatura.getCodigo() + "," + regularidad+ "\n";
    }

}
