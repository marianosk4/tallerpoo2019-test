package taller_1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author mariano
 */
public class Alumno extends Persona {

	private String domicilio;
	private String localidad;
	private String provincia;
	private String paisResidencia;
	private String email;
	private String matricula;
	private LocalDate fechaDeNacimiento;
	private LocalDate fechaDeInscripcionCarrera;
	private ArrayList<Carrera> ListaCarreras;
	private ArrayList<Asignatura> ListaAsignaturas;
	private ArrayList<Examen> ListaExamenes; 

	public Alumno(String nombre, String apellido, String dni,String domicilio, String localidad, String provincia,
	    String paisResidencia, String email, String matricula, 
	    LocalDate fechaDeNacimiento, LocalDate fechaDeInscripcionCarrera) {
		super(nombre, apellido, dni);
		this.domicilio = domicilio;
		this.localidad = localidad;
		this.provincia = provincia;
		this.paisResidencia = paisResidencia;
		this.email = email;
		this.matricula = matricula;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.fechaDeInscripcionCarrera = fechaDeInscripcionCarrera;
		this.ListaAsignaturas = new ArrayList();
		this.ListaCarreras = new ArrayList();
		this.ListaExamenes =  new ArrayList();
	}

	public ArrayList<Asignatura> getListaAsignaturas() {
		return ListaAsignaturas;
	}

	public void setListaAsignaturas(ArrayList<Asignatura> ListaAsignaturas) {
		this.ListaAsignaturas = ListaAsignaturas;
	}

	public ArrayList<Examen> getListaExamenes() {
		return ListaExamenes;
	}

	public void setListaExamenes(ArrayList<Examen> ListaExamenes) {
		this.ListaExamenes = ListaExamenes;
	}

	public void agregarCarrera(Carrera c){
		this.ListaCarreras.add(c);
	}
	
	public void agregarAsignatura (Asignatura a){
		this.ListaAsignaturas.add(a);
	}
	public ArrayList<Carrera> getListaCarreras() {
		return ListaCarreras;
	}

	public void setListaCarreras(ArrayList<Carrera> ListaCarreras) {
		this.ListaCarreras = ListaCarreras;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getPaisResidencia() {
		return paisResidencia;
	}

	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public LocalDate getFechaDeInscripcionCarrera() {
		return fechaDeInscripcionCarrera;
	}

	public void setFechaDeInscripcionCarrera(LocalDate fechaDeInscripcion) {
		this.fechaDeInscripcionCarrera = fechaDeInscripcion;
	}
 
	
	//a mano
	//@Override
	public boolean equals(Alumno a){
		return this.getDni().equals(a.getDni());
	}

	@Override
	public String toString() {
		return super.dni + " " + super.apellido + " " + super.nombre;
	}
	


	/**
         * retorna un String con el formato usado en el archivo alumnos.csv
         * @return String
         */
	
	//ver el tema de la regularidad
    public String aTabla() {
        return apellido + "," + nombre +"," + dni +"," + domicilio +"," + localidad + "," + provincia + "," + paisResidencia + "," + email + "," + matricula + ","  + fechaDeNacimiento + "," + fechaDeInscripcionCarrera + "," + ArrayListCSV(ListaCarreras,"CA:") + ArrayListCSV(ListaAsignaturas,"AS:") + "\n";
    }


	public String toCSV(){
        return  nombre+ "," +  apellido+ "," +  dni+ "," + domicilio+ "," +  localidad+ "," +  provincia+ "," +
	     paisResidencia+ "," +  email+ "," +  matricula+ "," + 
	     fechaDeNacimiento+ "," +  fechaDeInscripcionCarrera+ CarrerasCSV()+
	     "\n";
	}
        
	/**
	 * retorna un String en el formato CSV con 
	 * los datos del ArrayList pasado por parametro
	 * @param lista
	 * @param codigo
	 * @return String
	 */
	private String ArrayListCSV(ArrayList lista, String codigo) {
	String	resultado = "";

		for (Object c : lista) {
			resultado += codigo + c.toString() + ",";
		}
	
	return resultado;
	}

	private String CarrerasCSV() {
		String resultado = "";
		for (Carrera c : ListaCarreras) {
			resultado +="," + c.getCodigo();
		}
		return resultado;
	}


}
