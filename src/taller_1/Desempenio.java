package taller_1;

/**
 *
 * @author user
 */
public class Desempenio {

    private boolean asistencia;
    private Alumno alumno;
    private Examen examen;
    private int nota;

    public Desempenio(boolean asistencia, Alumno alumno, Examen examen, int nota) {
        this.asistencia = asistencia;
        this.alumno = alumno;
        this.examen = examen;
		if (asistencia) {	
			this.nota = nota;
		} else {
			this.nota=0;
		}
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public boolean getAsistencias() {
        return asistencia;
    }

    public void setAsistencias(boolean asistencias) {
        this.asistencia = asistencias;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Examen getExamen() {
        return examen;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }

    @Override
    public String toString() {
        return "Asistencias: " + asistencia
                + "\nAlumno: " + alumno
                + "\nExamen: " + examen
                + "\nNota: " + nota;
    }

	public String toCSV() {
		 return asistencia + "," +  alumno.getDni() + "," + examen.getFecha() + "," +examen.getMateria().getCodigo() + "," +nota+"\n";
	}

}
