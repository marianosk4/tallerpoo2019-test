package taller_1;

/**
 *
 * @author mariano
 */
public class Acta {
 private  boolean estado;
 private ExamenFinal examenFinal;

    public Acta(ExamenFinal examenFinal) {
        this.examenFinal = examenFinal;
        this.estado=true;
    }

	public ExamenFinal getExamenFinal() {
		return examenFinal;
	}
 
  

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Acta{" + "estado=" + estado + '}';
    }
 
	public String toCSV(){
		return examenFinal.getHorario() +","+ this.examenFinal.getFecha() + ","+ examenFinal.getMateria().getCodigo()+"\n";
	}
     
}
