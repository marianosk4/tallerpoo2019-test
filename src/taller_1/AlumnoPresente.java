package taller_1;

/**
 *
 * @author user
 */
public class AlumnoPresente {
	Alumno alumno;
	boolean presente;

	public AlumnoPresente(Alumno alumno, boolean presente) {
		this.alumno = alumno;
		this.presente = presente;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public boolean isPresente() {
		return presente;
	}

	public void setPresente(boolean presente) {
		this.presente = presente;
	}
	
	
}
