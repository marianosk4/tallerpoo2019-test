package taller_1;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author mariano
 */
public class ExamenFinal extends Examen {

    LocalTime horario;
    RolDocente[] roles;

    public ExamenFinal(LocalTime horario, LocalDate fecha, Asignatura materia) {
        super(fecha, materia);
        this.horario = horario;
        alumnos = new ArrayList();
        this.roles = new RolDocente[3];
    }

    public Docente getTitular() {
        return this.roles[0].getDocente();
    }

    public Docente getVocal1() {
        return this.roles[1].getDocente();
    }

    public Docente getVocal2() {
        return this.roles[2].getDocente();
    }

    public void setTitular(RolDocente r) {
        this.roles[0] = r;
    }

    public void setVocal1(RolDocente r) {
        this.roles[1] = r;
    }

    public void setVocal2(RolDocente r) {
        this.roles[2] = r;
    }

    public LocalTime getHorario() {
        return horario;
    }

    public void setHorario(LocalTime horario) {
        this.horario = horario;
    }

    @Override
    public String toString() {
        return "Final{" + "horario=" + horario + '}';
    }

    /**
     * devuelve un String con el formato usado en los archivos .csv
     *
     * @return
     */
    @Override
    public String toCSV() {
        return horario + "," + this.getFecha().toString() + "," + this.getMateria().getCodigo() + "\n";
    }

}
