package taller_1;

import java.time.LocalDate;

/**
 *
 * @author US
 */
public class InscripcionFinal {
    private LocalDate fecha;
    private Alumno alumno;
    private ExamenFinal examenFinal;

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public ExamenFinal getExamenFinal() {
        return examenFinal;
    }

    public void setExamenFinal(ExamenFinal examenFinal) {
        this.examenFinal = examenFinal;
    }

    public InscripcionFinal(LocalDate fecha, Alumno alumno, ExamenFinal examenFinal) {
        this.fecha = fecha;
        this.alumno = alumno;
        this.examenFinal = examenFinal;
		examenFinal.agregarAlumno(alumno);
    }

    public String toCSV(){
        return fecha+ "," +alumno.getDni() + "," + examenFinal.getMateria().getCodigo() + "," + examenFinal.getFecha()+"\n";
    }
}
