package taller_1;

import java.time.LocalDate;

/**
 *
 * @author mariano
 */
public class CargoAsignatura {
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String cargo;
    private String dedicacion;
    private Docente docente;
    private Asignatura asignatura;

	public CargoAsignatura(LocalDate fechaInicio, LocalDate fechaFin, String cargo, String dedicacion, Docente docente, Asignatura asignatura) {
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.cargo = cargo;
		this.dedicacion = dedicacion;
		this.docente = docente;
		this.asignatura = asignatura;
		this.asignatura.getListaDocentes().add(docente);
		this.docente.getListaAsignaturas().add(asignatura);

	}

	

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getDedicacion() {
        return dedicacion;
    }

    public void setDedicacion(String dedicacion) {
        this.dedicacion = dedicacion;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    @Override
    public String toString() {
        return "Cargo del Docente en una Asignatura: " + "\n Asignatura: " + asignatura+ "\n Docente: " + docente +"\n Fecha de inicio: " + fechaInicio + "\n Fecha fin: " + fechaFin + "\n Cargo: " + cargo + "\n Dedicacion: " + dedicacion;
    }

	public String toCSV() {
		return fechaInicio +"," +fechaFin+"," + cargo+","+ dedicacion+","+docente.getLegajo() + "," + asignatura.getCodigo() + "\n";
	}

    
    
    
    
}
