package taller_1;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author mariano
 */
public class Asignatura {
    private String nombre;
    private String codigo;
    private int cargaHorariaTotal;
    private int anio;
    private TipoAsignatura duracion;
    private TipoPromocion promocion;
    private int parciales;
    private ArrayList<Docente> listaDocentes;
    private ArrayList<PlanDeEstudio> listaPlanesDeEstudio;
    private ArrayList<Alumno> listaAlumnos;
    private ArrayList<Examen> listaExamenes;
	private ArrayList<Bitacora> listaBitacoras;
    
    public Asignatura(String nombre, String codigo, int cargaHorariaTotal, int anio, TipoAsignatura duracion, TipoPromocion promocion, int parciales) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.cargaHorariaTotal = cargaHorariaTotal;
        this.anio = anio;
        this.duracion = duracion;
        this.promocion = promocion;
        this.parciales = parciales;
		this.listaAlumnos = new ArrayList();
		this.listaDocentes= new ArrayList();
		this.listaExamenes = new ArrayList();
		this.listaPlanesDeEstudio = new ArrayList();
		this.listaBitacoras = new ArrayList();
	}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCargaHorariaTotal() {
        return cargaHorariaTotal;
    }

    public void setCargaHorariaTotal(int cargaHorariaTotal) {
        this.cargaHorariaTotal = cargaHorariaTotal;
    }

    public int getAño() {
        return anio;
    }

    public void setAño(int año) {
        this.anio = año;
    }

    public TipoAsignatura getDuracion() {
        return duracion;
    }

    public void setDuracion(TipoAsignatura duracion) {
        this.duracion = duracion;
    }

    public TipoPromocion getPromocion() {
        return promocion;
    }

    public void setPromocion(TipoPromocion promocion) {
        this.promocion = promocion;
    }

    public int getParciales() {
        return parciales;
    }

    public void setParciales(int parciales) {
        this.parciales = parciales;
    }

    public ArrayList<Docente> getListaDocentes() {
        return listaDocentes;
    }

    public void setListaDocentes(ArrayList<Docente> listaDocentes) {
        this.listaDocentes = listaDocentes;
    }

    public ArrayList<PlanDeEstudio> getListaPlanesDeEstudio() {
        return listaPlanesDeEstudio;
    }

    public void setListaPlanesDeEstudio(ArrayList<PlanDeEstudio> listaPlanesDeEstudio) {
        this.listaPlanesDeEstudio = listaPlanesDeEstudio;
    }

    public ArrayList<Alumno> getListaAlumnos() {
        return listaAlumnos;
    }

    public void setListaAlumnos(ArrayList<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }

    public ArrayList<Examen> getListaExamenes() {
        return listaExamenes;
    }

    public void setListaExamenes(ArrayList<Examen> listaExamenes) {
        this.listaExamenes = listaExamenes;
    }

	@Override
	public int hashCode() {
		int hash = 7;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Asignatura other = (Asignatura) obj;
		if (!Objects.equals(this.codigo, other.codigo)) {
			return false;
		}
		return true;
	}


    
	
    

    @Override
    public String toString() {
        return this.nombre;
    }
 
	public String aTabla(){
     return  nombre+","+ codigo+"," + cargaHorariaTotal+"," + anio+"," + duracion+"," + promocion+"," + parciales+","+ listaDocentes+","+ listaPlanesDeEstudio+","+ listaAlumnos+","+ listaExamenes+ "\n";
	}

	public String toCSV(){
		return nombre + "," + codigo +"," + cargaHorariaTotal+"," + anio + "," +duracion + "," + promocion + "," + parciales + "\n";
	}
    
    
}
