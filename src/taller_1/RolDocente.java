package taller_1;

/**
 *
 * @author mariano
 */
public class RolDocente {
    private CargoFinal cargo;
    private ExamenFinal examenFinal;
    private Docente docente;
    
    public RolDocente(CargoFinal cargo, ExamenFinal examenFinal,Docente docente) {
        this.cargo = cargo;
        this.examenFinal= examenFinal;
        this.docente=docente;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    
    public ExamenFinal getExamenFinal() {
        return examenFinal;
    }

    public void setExamenFinal(ExamenFinal examenFinal) {
        this.examenFinal = examenFinal;
    }

    public CargoFinal getCargo() {
        return cargo;
    }

    public void setCargo(CargoFinal cargo) {
        this.cargo = cargo;
    }

	
	public String aTabla() {
		return docente.getLegajo() +","+docente.getApellido() +","+ docente.getNombre() +"," +cargo;
	}
	
	public String toCSV(){
		return cargo+","+ examenFinal.getFecha()  +","+ examenFinal.getMateria().getCodigo() +","+docente.getLegajo()+"\n";
	}
    

}
