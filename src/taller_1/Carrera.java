package taller_1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author mariano
 */
public class Carrera {

	private String nombre;
	private String codigo;
	private LocalDate fechaDeCreacion;
	ArrayList<Alumno> listaAlumnos = new ArrayList();
	ArrayList<PlanDeEstudio> listaPlanesDeEstudio = new ArrayList();

	public Carrera(String nombre, String codigo, LocalDate fechaDeCreacion) {
		this.nombre = nombre;
		this.codigo = codigo;
		this.fechaDeCreacion = fechaDeCreacion;
	}

	public void agregarAlumno(Alumno a){
		this.listaAlumnos.add(a);
	}
	
	public ArrayList<Alumno> getListaAlumnos() {
		return listaAlumnos;
	}

	public void setListaAlumnos(ArrayList<Alumno> listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}

	
	public void agregarPlanDeEstudios(PlanDeEstudio p){
		listaPlanesDeEstudio.add(p);
	}
	
	public ArrayList<PlanDeEstudio> getListaPlanesDeEstudio() {
		return listaPlanesDeEstudio;
	}

	public void setListaPlanesDeEstudio(ArrayList<PlanDeEstudio> listaPlanesDeEstudio) {
		this.listaPlanesDeEstudio = listaPlanesDeEstudio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public LocalDate getFechaDeCreacion() {
		return fechaDeCreacion;
	}

	public void setFechaDeCreacion(LocalDate fechaDeCreacion) {
		this.fechaDeCreacion = fechaDeCreacion;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Carrera other = (Carrera) obj;
		if (!Objects.equals(this.codigo, other.codigo)) {
			return false;
		}
		return true;
	}

	
	
	
	
	//Este es para que aparezca solamente
	//el nombre en la lista
	@Override
	public String toString() {
		return nombre;
	}
	
	
	/**
	 * Devuelve una lista con formato CSV de los dni de los alumnos
	 * de listaAlumnos
	 * @return 
	 */
	private String alumnosCSV(){
		String resultado = "";
		for (Alumno a : listaAlumnos) {
			resultado += a.getDni() + ",";
		}
		return resultado;
	}
	
	/**
	 * Devuelve un String con el formato
	 * fechaImplementacion,asignatura_1,asignatura_2,...,asignatura_n
	 * @return 
	 */
	private String planesTabla(){
		String resultado = "";
		for (PlanDeEstudio p : listaPlanesDeEstudio) {
			resultado+=p.getFechaImplementacion()+",";
			for (Asignatura a : p.getListaAsignaturas()) {
				resultado += a.getCodigo() + ",";
			}
		}
		return resultado;
	}
	
	//Este es para agregar carreras csv
	//@Override
	//public String toString() {
	public String aTabla(){
		return nombre + "," +  codigo + "," + fechaDeCreacion+ planesTabla() + alumnosCSV() +"\n";
	}

	public String toCSV(){
		return nombre + "," + codigo + "," + fechaDeCreacion + "\n";
	}
}
