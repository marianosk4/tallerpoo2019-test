package taller_1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author mariano
 */
public class Examen {

	private LocalDate fecha;
	private Asignatura materia;
	ArrayList<Alumno> alumnos;
	boolean abierto;
	
	public Examen(LocalDate fecha, Asignatura materia) {
		this.fecha = fecha;
		this.materia = materia;
		this.alumnos = new ArrayList();
		this.abierto=true;
	}

	public boolean isAbierto() {
		return abierto;
	}

	public void setAbierto(boolean estado) {
		this.abierto = estado;
	}
	
	

	public void agregarAlumno(Alumno a) {
		alumnos.add(a);
	}

	public ArrayList<Alumno> getAlumnos() {
		return alumnos;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Asignatura getMateria() {
		return materia;
	}

	public void setMateria(Asignatura materia) {
		this.materia = materia;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Examen other = (Examen) obj;
		if (!Objects.equals(this.fecha, other.fecha)) {
			return false;
		}
		if (!Objects.equals(this.materia, other.materia)) {
			return false;
		}
		return true;
	}
	
	

	@Override
	public String toString() {
		return "Examen{" + "fecha=" + fecha + ", materia=" + materia + '}';
	}

	public String toCSV() {
		return fecha + "," + materia.getCodigo() + alumnosCSV() + "\n";
	}

	private String alumnosCSV() {
		String resultado = "";
		for (Alumno alumno : alumnos) {
			resultado += "," + alumno.getDni();
		}
		return resultado;
	}

}
