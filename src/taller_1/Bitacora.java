package taller_1;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author mariano
 */
public class Bitacora {
    private LocalDate fecha;
    private String temasDados;
	private Asignatura asignatura;
    private ArrayList<Docente> listaDocentes;
	private ArrayList<AlumnoPresente> presentismo;    

    public Bitacora(LocalDate fecha, String temasDados, Asignatura asignatura, Docente docente) {
        this.fecha = fecha;
		this.asignatura = asignatura;
        this.temasDados = temasDados;
		listaDocentes = new ArrayList();
		listaDocentes.add(docente);
		presentismo = new ArrayList();
		crearListaAlumnoPresente();
    }

	public ArrayList<AlumnoPresente> getPresentismo() {
		return presentismo;
	}

	public ArrayList<Alumno> listarAlumnos(){
		return this.asignatura.getListaAlumnos();
	}

	
	/**
	 * Crea la lista de alumnos con el presente como falso por default
	 */
	private void crearListaAlumnoPresente(){
		for (Alumno a : asignatura.getListaAlumnos()) {
			presentismo.add(new AlumnoPresente(a,false));
		}
	}
	
	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	
    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getTemasDados() {
        return temasDados;
    }

	public void agregarTema(String nuevoTema){
		this.temasDados+= "|" +nuevoTema;
	}

    public void setTemasDados(String temasDados) {
        this.temasDados = temasDados;
    }

    @Override
    public String toString() {
        return "Bitacora{" + "fecha=" + fecha + ", temasDados=" + temasDados + '}';
    }

	public String toCSV() {
    return fecha + "," + temasDados + "," + asignatura.getCodigo() + docentesCSV()+ alumnoPresenteCSV()+"\n";
	
	}
	
	String docentesCSV(){
		String s =  "";

		for (Docente d: listaDocentes) {
			s += ","+d.getLegajo();
		}
		
		return s;
	}
    
    
	/**
	 * devuelve un String del presente de los alumnos con el formato dni,boolean
	 * @return 
	 */
	String alumnoPresenteCSV(){
		String s =  "";
		for (AlumnoPresente p : presentismo) {
			s +=","+p.getAlumno().getDni()+","+p.isPresente();
		}
		
		return s;
	}
	
	
}
