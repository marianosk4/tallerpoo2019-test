package taller_1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author mariano
 */
public class PlanDeEstudio {

	private LocalDate fechaImplementacion;//identificador
	private LocalDate fechaTermina;
	private int cantAnios;
	private Carrera carrera;
	private ArrayList<Asignatura> listaAsignaturas = new ArrayList();

	public PlanDeEstudio(LocalDate fechaImplementacion, LocalDate fechaTermina, int cantAños, Carrera carrera) {
		this.fechaImplementacion = fechaImplementacion;
		this.fechaTermina = fechaTermina;
		this.cantAnios = cantAños;
		this.carrera = carrera;
	}

	public void agregarAsignatura(Asignatura a) {
		listaAsignaturas.add(a);
	}

	public ArrayList<Asignatura> getListaAsignaturas() {
		return listaAsignaturas;
	}

	public void setListaAsignaturas(ArrayList<Asignatura> listaAsignaturas) {
		this.listaAsignaturas = listaAsignaturas;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public LocalDate getFechaImplementacion() {
		return fechaImplementacion;
	}

	public void setFechaImplementacion(LocalDate fechaImplementacion) {
		this.fechaImplementacion = fechaImplementacion;
	}

	public LocalDate getFechaTermina() {
		return fechaTermina;
	}

	public void setFechaTermina(LocalDate fechaTermina) {
		this.fechaTermina = fechaTermina;
	}

	public int getCantAños() {
		return cantAnios;
	}

	public void setCantAños(int cantAños) {
		this.cantAnios = cantAños;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PlanDeEstudio other = (PlanDeEstudio) obj;
		if (!Objects.equals(this.fechaImplementacion, other.fechaImplementacion)) {
			return false;
		}
		if (!Objects.equals(this.carrera, other.carrera)) {
			return false;
		}
		return true;
	}


	



	@Override
	public String toString() {
		return String.valueOf(fechaImplementacion.getYear());
	}

	public String toCSV() {
		return fechaImplementacion + "," + fechaTermina + "," + cantAnios + "," + carrera.getCodigo() + listarAsignaturasCSV() + "\n";

	}

	private String listarAsignaturasCSV() {
		String resultado ="";
		for (Asignatura a : listaAsignaturas) {
			resultado+= "," +a.getCodigo();
		}
		return resultado;
	}

}
