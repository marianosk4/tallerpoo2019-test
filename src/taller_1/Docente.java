package taller_1;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author mariano
 */
public class Docente extends Persona {

    private int legajo;
    private LocalDate fechaIngreso;
    private ArrayList<Asignatura> listaAsignaturas;
    private ArrayList<Acta> listaActas;
    private ArrayList<Bitacora> listaBitacoras;

    public Docente(String nombre, String apellido, String dni, int legajo, LocalDate fechaIngreso) {
        super(nombre, apellido, dni);
        this.legajo = legajo;
        this.fechaIngreso = fechaIngreso;
        this.listaAsignaturas = new ArrayList();
        this.listaActas = new ArrayList();
        this.listaBitacoras = new ArrayList();
    }

    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public ArrayList<Asignatura> getListaAsignaturas() {
        return listaAsignaturas;
    }

    public void setListaAsignaturas(ArrayList<Asignatura> listaAsignaturas) {
        this.listaAsignaturas = listaAsignaturas;
    }

    public ArrayList<Acta> getListaActas() {
        return listaActas;
    }

    public void setListaActas(ArrayList<Acta> listaActas) {
        this.listaActas = listaActas;
    }

    public ArrayList<Bitacora> getListaBitacoras() {
        return listaBitacoras;
    }

    public void setListaBitacoras(ArrayList<Bitacora> listaBitacoras) {
        this.listaBitacoras = listaBitacoras;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Docente other = (Docente) obj;
        if (this.legajo != other.legajo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Integer.toString(legajo) + " " + this.apellido + " " + this.nombre;
    }

    public String toCSV() {
        return nombre + "," + apellido + "," + dni + "," + legajo + "," + fechaIngreso + "\n";
    }

}
