/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller_1;

import interfaz.Inicio;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mariano
 */
public class Taller_1 {

    /**
     * @param args the command line arguments
     * @throws FileNotFoundException si no encuentra los archivos
     */
    public static void main(String[] args) throws FileNotFoundException {

        // CreandoListas a partir De Archivos
        ArrayList<Carrera> carreras = crearListaCarreras();
		
        ArrayList<Alumno> alumnos = crearListaAlumnos(carreras);
		
        ArrayList<Docente> docentes = crearListaDocentes();
		
        ArrayList<Asignatura> asignaturas = crearListaAsignaturas();

        ArrayList<PlanDeEstudio> planes = crearListaPlanes(carreras, asignaturas);

        ArrayList<CargoAsignatura> cargos = crearListaCargos(docentes, asignaturas);
		


        ArrayList<Inscripcion> inscripciones = crearListaInscripciones(alumnos, asignaturas);

        ArrayList<Bitacora> bitacoras = crearListaBitacoras(asignaturas, docentes);

        ArrayList<Parcial> parciales = crearListaParciales(asignaturas);
        ArrayList<Recuperatorio> recuperatorios = creaListaRecuperatorios(asignaturas);
        ArrayList<Desempenio> desempenios = crearListaDesempenios(alumnos, parciales, recuperatorios);

		
		ArrayList<ExamenFinal> examenesFinales = crearListaExamenesFinales(asignaturas);
		ArrayList<Examen> examenes = new ArrayList();
      
		
		for (ExamenFinal e : examenesFinales) {
			examenes.add(e);
		}
		
		for (Parcial p : parciales) {
			examenes.add(p);
		}
		for (Recuperatorio recuperatorio : recuperatorios) {
			examenes.add(recuperatorio);
		}
		
      
		ArrayList<RolDocente> roles = crearListaRoles(examenesFinales, asignaturas,docentes);
        
		
		//Acta acta = new Acta(examenFinal);
		ArrayList<Acta> actas = cargarListaActas(examenesFinales,asignaturas);
		//actas.add(acta);
		ArrayList<InscripcionFinal> inscripcionesFinales = crearListainscripcionesFinales(examenesFinales,alumnos,asignaturas);
        
        new Inicio(alumnos, carreras, docentes, bitacoras, asignaturas, cargos, examenes, desempenios, inscripciones,roles,planes,actas,inscripcionesFinales).setVisible(true);

    }

    /**
     * Crea un lista de alumnos a partir del archivo alumnos.csv Asignando la
     * carrera (de lista de carreras) por el codigo en dicho archivo
     *
     * @param carreras lista de carreras
     * @return ArrayList lista de alumnos
     * @throws FileNotFoundException a
     */
    public static ArrayList<Alumno> crearListaAlumnos(ArrayList<Carrera> carreras) throws FileNotFoundException {

        ArrayList<Alumno> a = new ArrayList();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/alumnos.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                Alumno alumno = new Alumno(c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7], c[8], LocalDate.parse(c[9], formatter), LocalDate.parse(c[10], formatter));

                for (Carrera carrera : carreras) {
                    for (int i = 0; i < c.length; i++) {
                        if (c[i].equals(carrera.getCodigo())) {
                            alumno.agregarCarrera(carrera);
                        }
                    }
                }

                a.add(alumno);
                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }

    /**
     * Crea la lista de docentes a partir de docentes.csv
     *
     * @return
     * @throws FileNotFoundException
     */
    public static ArrayList<Docente> crearListaDocentes() throws FileNotFoundException {

        ArrayList<Docente> d = new ArrayList();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/docentes.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                d.add(new Docente(c[0], c[1], c[2], Integer.parseInt(c[3]), LocalDate.parse(c[4], formatter)));

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return d;
    }

    /**
     * Crea la lista de carreras a partir de carreras.csv
     *
     * @return arraylist de carreras
     * @throws FileNotFoundException si no se encontraron los archivos
     */
    public static ArrayList<Carrera> crearListaCarreras() throws FileNotFoundException {

        ArrayList<Carrera> carreras = new ArrayList();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/carreras.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                carreras.add(new Carrera(c[0], c[1], LocalDate.parse(c[2], formatter)));

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carreras;
    }

    /**
     * crea la lista de asignaturas a partir de asignaturas.csv
     *
     * @return arraylist de asignaturas
     * @throws FileNotFoundException si no se encontraron los archivos
     */
    public static ArrayList<Asignatura> crearListaAsignaturas() throws FileNotFoundException {

        ArrayList<Asignatura> a = new ArrayList();
        try {
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/asignaturas.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                a.add(new Asignatura(c[0], c[1], Integer.parseInt(c[2]), Integer.parseInt(c[3]), TipoAsignatura.valueOf(c[4]), TipoPromocion.valueOf(c[5]), Integer.parseInt(c[6])));

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }

    /**
     * Crea una lista de planes de estudio a partir del archivo
     * planesEstudio.csv, una lista de Carreras y una lista de Asignaturas
     * (ambos guardados en planesEstudio.csv);
     *
     * @param carreras lista de carreras
     * @return arraylist de  PlanDeEstudio
     * @throws FileNotFoundException si no se encontraron los archivos
     */
    public static ArrayList<PlanDeEstudio> crearListaPlanes(ArrayList<Carrera> carreras, ArrayList<Asignatura> asignaturas) throws FileNotFoundException {

        ArrayList<PlanDeEstudio> p = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/planesEstudio.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                PlanDeEstudio plan = new PlanDeEstudio(LocalDate.parse(c[0], formatter), LocalDate.parse(c[1], formatter), Integer.parseInt(c[2]), buscarCarrera(carreras, c[3]));

                //recorro la lista de asignaturas y busco coincidencias con la lista de Asignaturas del plan
                // Se que en el archivo, los campos 0 a 3 tienen los datos pasados
                // al momento de crear el nuevo plan (ver linea anterior)
                // y a partir del campo 4 empiezan los codigos de las asignaturas correspondientes
                for (Asignatura asignatura : asignaturas) {
                    for (int i = 4; i < c.length; i++) {
                        if (asignatura.getCodigo().equals(c[i])) {
                            plan.agregarAsignatura(asignatura);
                        }
                    }
                }

                //asigno el plan a su respectiva carrera
                for (Carrera carrera : carreras) {
                    if (plan.getCarrera().equals(carrera)) {
                        carrera.agregarPlanDeEstudios(plan);
                    }
                }

                //agrego el plan a la lista de Planes
                p.add(plan);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    /**
     * Busca en la lista de carreras el codigo pasado por parametro Si hubo
     * coincidencia, devuelve la carrera
     *
     * @param carreras lista carreras
     * @param codigo codigo de la carrera buscada
     * @return
     */
    private static Carrera buscarCarrera(ArrayList<Carrera> carreras, String codigo) {
        Carrera car = null;

        for (Carrera c : carreras) {
            if (c.getCodigo().equals(codigo)) {
                car = c;
            }
        }

        return car;
    }

    /**
     * Crea una lista de cargoDocente, donde se asocian los docentes y las
     * asignaturas en las que tiene cargos
     *
     * @param docentes lista docentes
     * @param asignaturas lista asignaturas
     * @return
     */
    private static ArrayList<CargoAsignatura> crearListaCargos(ArrayList<Docente> docentes, ArrayList<Asignatura> asignaturas) {
        ArrayList<CargoAsignatura> cargos = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/cargosAsignaturas.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                CargoAsignatura cargo = new CargoAsignatura(LocalDate.parse(c[0], formatter), LocalDate.parse(c[1], formatter), c[2], c[3], buscarDocente(docentes, c[4]), buscarAsignatura(asignaturas, c[5]));

                cargos.add(cargo);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cargos;
    }

    /**
     * Busca en la lista de docentes el docente con el codigo pasado por
     * parametro
     *
     * @param docentes lista docentes
     * @param codigo Legajo del docente buscado
     * @return
     */
    private static Docente buscarDocente(ArrayList<Docente> docentes, String codigo) {
        Docente docente = null;

        for (Docente d : docentes) {
            if (d.getLegajo() == Integer.parseInt(codigo)) {
                docente = d;
            }
        }

        return docente;
    }

    /**
     * busca en la lista de asignaturas la asignatura correspondiente al codigo
     * pasado por parametros
     *
     * @param asignaturas lista asignaturas
     * @param codigo codigo de la asignatura
     * @return
     */
    private static Asignatura buscarAsignatura(ArrayList<Asignatura> asignaturas, String codigo) {
        Asignatura asignatura = null;

        for (Asignatura a : asignaturas) {
            if (a.getCodigo().equals(codigo)) {
                asignatura = a;
            }
        }

        return asignatura;
    }

    /**
     * Busca el alumno con el codigo (dni) pasado por parametros
     *
     * @param alumnos lista de alumnos
     * @param codigo dni del alumno
     * @return
     */
    private static Alumno buscarAlumno(ArrayList<Alumno> alumnos, String codigo) {
        Alumno alumno = null;

        for (Alumno a : alumnos) {
            if (a.getDni().equals(codigo)) {
                alumno = a;
            }
        }

        return alumno;
    }
	
		/**
	 * Busca el exaneb final comparando la asignatura y la fecha con los de 
	 * los examenes guardados en la lista de examenes finales
	 * @param examenes lista de examenes finales
	 * @param asignatura asignatura del examen buscado
	 * @param fecha fecha del examen buscado
	 * @return  un ExamenFinal
	 */          
    private static ExamenFinal buscarExamenFinal(ArrayList<ExamenFinal> examenes, Asignatura asignatura,LocalDate fecha ) {
        ExamenFinal examenFinal = null;
		
		for (ExamenFinal e : examenes) {
			if (e.getFecha().equals(fecha)) {
				if (e.getMateria().equals(asignatura)) {
					examenFinal = e;
				}
			}
		}
		
        return examenFinal;
    }
	
	
	

    /**
     * Crea una lista de inscripciones a partir dek archivo inscripciones.csv,
     * creando a su vez una inscripcion en la cual se asocia al alumno con la
     * asignatura que va a cursar
     *
     * @param alumnos lista de alumnos
     * @param asignaturas lista de asignaturas
     * @return
     */
    private static ArrayList<Inscripcion> crearListaInscripciones(ArrayList<Alumno> alumnos, ArrayList<Asignatura> asignaturas) {

        ArrayList<Inscripcion> inscripciones = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/inscripciones.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                Inscripcion inscripcion = new Inscripcion(LocalDate.parse(c[0], formatter), c[1], buscarAlumno(alumnos, c[2]), buscarAsignatura(asignaturas, c[3]));
			 //  Inscripcion inscripcion =new Inscripcion(LocalDate.parse(c[0], formatter), c[1], alumnos.get(0),asignaturas.get(0));
			   
                inscripciones.add(inscripcion);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inscripciones;
    }

    /**
     * Crea una lista de bitacoras a partir de bitacoras.csv las bitacoras se
     * usan para obtener el presentismo de los alumnos para ver si estan
     * habilitados para rendir el parcial
     *
     * @param asignaturas lista de asignaturas
     * @param docentes lista de docentes
     * @return
     */
    private static ArrayList<Bitacora> crearListaBitacoras(ArrayList<Asignatura> asignaturas, ArrayList<Docente> docentes) {
        ArrayList<Bitacora> bitacoras = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/bitacoras.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                Bitacora bitacora = new Bitacora(LocalDate.parse(c[0], formatter), c[1], buscarAsignatura(asignaturas, c[2]), buscarDocente(docentes, c[3]));

                //cargo los presentes 
                for (AlumnoPresente ap : bitacora.getPresentismo()) {
                    for (int i = 0; i < c.length; i++) {
                        if (i < c.length - 1 && c[i].equals(ap.getAlumno().getDni())) {
                            ap.setPresente(Boolean.parseBoolean(c[i + 1]));
                        }
                    }
                }

                bitacoras.add(bitacora);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bitacoras;

    }

    /**
     * Crea una lista de parciales a partir de los parciales guardados en
     * parciales.csv
     *
     * @param asignaturas lista de asignaturas
     * @return lista de parciales
     */
    private static ArrayList<Parcial> crearListaParciales(ArrayList<Asignatura> asignaturas) {
        ArrayList<Parcial> parciales = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/parciales.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                Parcial parcial = new Parcial(LocalDate.parse(c[0], formatter), buscarAsignatura(asignaturas, c[1]));

                parciales.add(parcial);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parciales;
    }

    /**
     * Crea una lista de recuperatorios a partir de los parciales guardados en
     * parciales.csv
     *
     * @param asignaturas lista de asignatura
     * @param recuperatorios lista de recuperatorio
     * @return
     */
    private static ArrayList<Recuperatorio> creaListaRecuperatorios(ArrayList<Asignatura> asignaturas) {
        ArrayList<Recuperatorio> recuperatorios = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/recuperatorios.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");

                Recuperatorio recuperatorio = new Recuperatorio(LocalDate.parse(c[0], formatter), buscarAsignatura(asignaturas, c[1]));

                recuperatorios.add(recuperatorio);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return recuperatorios;
    }

    /**
     * Crea una lista de Desempenio a partir del archivo desempenios.csv, en
     * desempenios.csv se guardan los desempenios de todos los
     * examentes(parciales y recuperatorios) Si el aumno reprobo el Parcial, no
     * se agrega el desempenio del mismo. sino que directamente se agrega el
     * desempenio del recuperatorio a la lista (salga bien o mal en el
     * recuperatorio)
     *
     * @param alumnos
     * @param parciales
     * @param recuperatorios
     * @return
     */
    private static ArrayList<Desempenio> crearListaDesempenios(ArrayList<Alumno> alumnos, ArrayList<Parcial> parciales, ArrayList<Recuperatorio> recuperatorios) {
        ArrayList<Desempenio> desempenios = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/desempenios.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");
				
				Desempenio desempenio = new Desempenio(Boolean.parseBoolean(c[0]), buscarAlumno(alumnos, c[1]), buscarExamen(parciales, recuperatorios, c[2], c[3]), Integer.parseInt(c[4]));

                //Si el examen es un parcial, y la nota es menor que 6, no 
                //lo agrego a la lista, agrego solamente la nota del Recuperatorio
                if (desempenio.getExamen() instanceof taller_1.Parcial && Integer.parseInt(c[4]) > 5
                        || desempenio.getExamen() instanceof taller_1.Recuperatorio) {
                    desempenios.add(desempenio);
                }

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return desempenios;
    }

    /**
     * A este metodo se lo llama desde crearListaDesempenios, busca en las
     * listas de Parciales y Recuperatorios el examen correspondiente a la fecha
     * del desempenio
     *
     * @param parciales
     * @param recuperatorios
     * @param fecha
     * @param codAsignatura
     * @return
     */
    private static Examen buscarExamen(ArrayList<Parcial> parciales, ArrayList<Recuperatorio> recuperatorios, String fecha, String codAsignatura) {

        Examen examen = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        for (Parcial p : parciales) {

            if (p.getFecha().equals(LocalDate.parse(fecha, formatter)) && p.getMateria().getCodigo().equals(codAsignatura)) {
                examen = p;
            }
        }

        for (Recuperatorio r : recuperatorios) {
            if (r.getFecha().equals(LocalDate.parse(fecha, formatter)) && r.getMateria().getCodigo().equals(codAsignatura)) {
                examen = r;
            }
        }

        return examen;
    }

		/**
	 * carga desde la base de datos los datos de los examenes guardados
	 * @param asignaturas lista asignaturas
	 * @return arraylist de ExamenFinal
	 */
	private static ArrayList<ExamenFinal> crearListaExamenesFinales(ArrayList<Asignatura> asignaturas) {
	ArrayList<ExamenFinal> examenesFinales = new ArrayList();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			 DateTimeFormatter horaFormatter = DateTimeFormatter.ofPattern("HH:mm");
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/examenesFinales.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");
				
				ExamenFinal examenFinal = new ExamenFinal(LocalTime.parse(c[0],horaFormatter),
					LocalDate.parse(c[1], formatter),buscarAsignatura(asignaturas,c[2]));
					
               examenesFinales.add(examenFinal);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return examenesFinales;	
	
	}

		/**
	 * carga los Roles de los docentes en los examenes finales desde el archivo roles.csv
	 * @param examenesFinales lista de examenesfinales
	 * @param asignaturas lista de asignaturas
	 * @param docentes lista de docentes
	 * @return arraylist de RolDocente
	 */
	private static ArrayList<RolDocente> crearListaRoles(ArrayList<ExamenFinal> examenesFinales, ArrayList<Asignatura> asignaturas,ArrayList<Docente> docentes) {
			ArrayList<RolDocente> roles = new ArrayList();

        try {
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/roles.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");
				Asignatura asignaturaDelExamen = buscarAsignatura(asignaturas,c[2]);
				
				RolDocente rol = new RolDocente(CargoFinal.valueOf(c[0]), 
					buscarExamenFinal(examenesFinales, asignaturaDelExamen , LocalDate.parse(c[1])), 
					buscarDocente(docentes,c[3]));
				
					
				 roles.add(rol);

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return roles;
	
	}

		/**
	 * Carga en la lista de actas los elementos guardados en el archivo actas.csv
	 * @param examenesFinales lista de finales, para poder ubicar el examen del acta
	 * @param asignaturas lista asignaturas
	 * @return arraylist de Acta
	 */
	private static ArrayList<Acta> cargarListaActas(ArrayList<ExamenFinal> examenesFinales, ArrayList<Asignatura> asignaturas) {
		ArrayList<Acta> actas = new ArrayList();
        try {
			
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/actas.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");
				
				ExamenFinal examenDelActa = buscarExamenFinal(examenesFinales, buscarAsignatura(asignaturas, c[2]),LocalDate.parse(c[1]));
				actas.add(new Acta(examenDelActa));
					
				 

                linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return actas;
	
	}

	/**
	 * Crea la lista de InscripcionFinal
	 * @param examenesFinales la lista de examenes
	 * @param alumnos lista de alumnos,
	 * @param asignaturas lista de asignaturas
	 * @return arraylist de InscripcionFinal
	 */
	private static ArrayList<InscripcionFinal> crearListainscripcionesFinales(ArrayList<ExamenFinal> examenesFinales, ArrayList<Alumno> alumnos, ArrayList<Asignatura> asignaturas) {
				ArrayList<InscripcionFinal> inscripcionesFinales = new ArrayList();
				
        try {
			
            BufferedReader br = null;
            br = new BufferedReader(new FileReader("csv/inscripcionesFinales.csv"));

            String linea = br.readLine();
            while (linea != null) {
                String[] c = linea.split(",");
				
				ExamenFinal examenDelActa = buscarExamenFinal(examenesFinales, buscarAsignatura(asignaturas, c[2]),LocalDate.parse(c[3]));
				
				inscripcionesFinales.add(new InscripcionFinal(LocalDate.parse(c[0]), buscarAlumno(alumnos, c[1]), examenDelActa));
				
				linea = br.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(Taller_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inscripcionesFinales;
	
	}
}
