package interfaz;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import taller_1.Acta;
import taller_1.Alumno;
import taller_1.Asignatura;
import taller_1.Bitacora;
import taller_1.CargoAsignatura;
import taller_1.Carrera;
import taller_1.Desempenio;
import taller_1.Docente;
import taller_1.Examen;
import taller_1.Inscripcion;
import taller_1.InscripcionFinal;
import taller_1.PlanDeEstudio;
import taller_1.RolDocente;
import taller_1.TipoRegularidad;

/**
 *
 * @author Curbelo y Dirié.
 */
public class AlumnosExamenFinal extends javax.swing.JFrame {

    ArrayList<Alumno> alumnos;
    ArrayList<Carrera> carreras;
    ArrayList<Docente> docentes;
    ArrayList<Bitacora> bitacoras;
    ArrayList<Asignatura> asignaturas;
    ArrayList<CargoAsignatura> cargosAsignaturas;
    ArrayList<Examen> examenes;
    ArrayList<Desempenio> desempenios;
    ArrayList<Inscripcion> inscripciones;
    ArrayList<RolDocente> roles;
    ArrayList<PlanDeEstudio> planes;
    ArrayList<Acta> actas;
		ArrayList<InscripcionFinal> inscripcionesFinales;

    /**
     * Constructor por defecto.
     */
    public AlumnosExamenFinal() {
        initComponents();
    }

    /**
     * Constructor por parámetro.
     *
     * @param alumnos lista de alumnos
     * @param carreras lista de carreras
     * @param docentes lista de docentes
     * @param bitacoras lista de bitacoras
     * @param asignaturas lista de asignaturas
     * @param cargosAsignaturas lista de cargosAsignatruas
     * @param examenes lista de examentes
     * @param desempenios lista de desempenios
     * @param inscripciones lista de inscripciones
     * @param roles lista de roles
     * @param planes lista de planes
     * @param actas lista de actas.
     */
    public AlumnosExamenFinal(ArrayList<Alumno> alumnos, ArrayList<Carrera> carreras, ArrayList<Docente> docentes, ArrayList<Bitacora> bitacoras, ArrayList<Asignatura> asignaturas, ArrayList<CargoAsignatura> cargosAsignaturas, ArrayList<Examen> examenes, ArrayList<Desempenio> desempenios, ArrayList<Inscripcion> inscripciones, ArrayList<RolDocente> roles, ArrayList<PlanDeEstudio> planes, ArrayList<Acta> actas,	ArrayList<InscripcionFinal> inscripcionesFinales) {
        this.alumnos = alumnos;
        this.carreras = carreras;
        this.docentes = docentes;
        this.bitacoras = bitacoras;
        this.asignaturas = asignaturas;
        this.cargosAsignaturas = cargosAsignaturas;
        this.examenes = examenes;
        this.desempenios = desempenios;
        this.inscripciones = inscripciones;
        this.roles = roles;
        this.planes = planes;
        this.actas = actas;
		this.inscripcionesFinales=inscripcionesFinales;
		initComponents();
        customComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        listaAsignatura = new javax.swing.JComboBox<>();
        listaPlan = new javax.swing.JComboBox<>();
        listaCarrera = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaAlumnos = new javax.swing.JTable();
        botonVolver = new javax.swing.JButton();
        periodo = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Carrera");

        jLabel2.setText("Plan");

        jLabel3.setText("Asignatura");

        jLabel4.setText("Periodo");

        listaPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaPlanActionPerformed(evt);
            }
        });

        listaCarrera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaCarreraActionPerformed(evt);
            }
        });

        tablaAlumnos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaAlumnos);

        botonVolver.setText("Volver");
        botonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonVolverActionPerformed(evt);
            }
        });

        periodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                periodoActionPerformed(evt);
            }
        });

        botonBuscar.setText("Buscar");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        jButton1.setText("Crear Examen Final");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel3))
                                .addGap(39, 39, 39)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(listaAsignatura, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(listaCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(47, 47, 47)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel2))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(19, 19, 19)
                                        .addComponent(listaPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(periodo, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(botonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 642, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(33, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botonVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(62, 62, 62))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(listaAsignatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(periodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(listaPlan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(listaCarrera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonVolver)
                    .addComponent(jButton1))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonVolverActionPerformed
        this.setVisible(false);
        new BedeliaHome(alumnos, carreras, docentes, bitacoras, asignaturas, cargosAsignaturas, examenes, desempenios, inscripciones, roles, planes, actas,inscripcionesFinales).setVisible(true);
    }//GEN-LAST:event_botonVolverActionPerformed

    private void periodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_periodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_periodoActionPerformed

    private void listaCarreraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaCarreraActionPerformed
        limpiarListas();
        armarListaPlanes((Carrera) listaCarrera.getSelectedItem());
    }//GEN-LAST:event_listaCarreraActionPerformed

    /**
     * Arma la lista de Asignaturas a partir del item seleccionado, si no hay un
     * item seleccionado, tira null pointer
     *
     * @param evt
     */
    private void listaPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaPlanActionPerformed
        if (listaPlan.getSelectedItem() != null) {
            armarListaAsignaturas((PlanDeEstudio) listaPlan.getSelectedItem());
        }
    }//GEN-LAST:event_listaPlanActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        if (listaAsignatura.getSelectedItem() != null) {
            armarTablaAlumnos((Asignatura) listaAsignatura.getSelectedItem());
        }
    }//GEN-LAST:event_botonBuscarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new NuevoExamen(examenes, docentes, roles, actas, (Asignatura) listaAsignatura.getSelectedItem(), true).setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AlumnosExamenFinal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AlumnosExamenFinal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AlumnosExamenFinal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AlumnosExamenFinal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AlumnosExamenFinal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonVolver;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<Asignatura> listaAsignatura;
    private javax.swing.JComboBox<Carrera> listaCarrera;
    private javax.swing.JComboBox<PlanDeEstudio> listaPlan;
    private javax.swing.JTextField periodo;
    private javax.swing.JTable tablaAlumnos;
    // End of variables declaration//GEN-END:variables

    /**
     * Agrega la lista de carreras al JComboBox "listaCarrera".
     */
    private void customComponents() {
        for (Carrera c : carreras) {
            this.listaCarrera.addItem(c);
        }
    }

    /**
     * Agrega la lista de planes de estudio al JComboBox "listaPlan".
     *
     * @param c Variable de tipo Carrera.
     */
    private void armarListaPlanes(Carrera c) {
        for (PlanDeEstudio planDeEstudio : c.getListaPlanesDeEstudio()) {
            listaPlan.addItem(planDeEstudio);
        }
    }

    /**
     * Agrega la lista de asignaturas al JComboBox "listaAsignatura".
     *
     * @param p Variable de tipo PlanDeEstudio.
     */
    private void armarListaAsignaturas(PlanDeEstudio p) {
        for (Asignatura a : p.getListaAsignaturas()) {
            listaAsignatura.addItem(a);
        }
    }

    /**
     * Vacia las listas de Plan y Asignatura cuando se cambia de carrera
     */
    private void limpiarListas() {
        this.listaPlan.removeAllItems();
        this.listaAsignatura.removeAllItems();
    }

    /**
     * Arma la tabla de alumnos segun su promedio si el promedio , no lo agrega
     * y quita su regularidad.
     *
     * @param asignatura Variable de tipo Asignatura.
     */
    private void armarTablaAlumnos(Asignatura asignatura) {
        String[] filas = {"Nombre", "Apellido", "DNI", "Carrera", "Localidad",
            "Provincia", "Pais", "email", "matricula",
            "Fecha Nacimiento", "FechaInscripcion", "regularidad"};
        DefaultTableModel datos = new DefaultTableModel(filas, 0);
        tablaAlumnos.setModel(datos);

        for (Alumno alumno : asignatura.getListaAlumnos()) {
            String[] campos = alumno.aTabla().split(",");
            datos.addRow(campos);

        }
    }

}
