package interfaz;

import javax.swing.table.DefaultTableModel;

/**
 * JTableModel con las celdas ineditables
 * lo usamos en PrintPrev, para que al imprimir el acta no sean editables 
 * las celdas de la ventana
 * @author mariano
 */
public class TableModelIneditable extends DefaultTableModel {

 	
	public TableModelIneditable(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}
	
	

 	@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}	
}
