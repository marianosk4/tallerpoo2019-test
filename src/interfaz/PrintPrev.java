package interfaz;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import taller_1.Acta;
import taller_1.Alumno;
import taller_1.Asignatura;
import taller_1.Bitacora;
import taller_1.CargoAsignatura;
import taller_1.Carrera;
import taller_1.Desempenio;
import taller_1.Docente;
import taller_1.Examen;
import taller_1.Inscripcion;
import taller_1.PlanDeEstudio;
import taller_1.RolDocente;

/**
 * Crea una ventana con las tablas de alumno y docente en un mismo jPanel para
 * poder imprimir el Acta
 *
 * @author Curbelo y Dirié.
 */
public class PrintPrev extends javax.swing.JFrame {

	ArrayList<Alumno> alumnos;
	ArrayList<Carrera> carreras;
	ArrayList<Docente> docentes;
	ArrayList<Bitacora> bitacoras;
	ArrayList<Asignatura> asignaturas;
	ArrayList<CargoAsignatura> cargosAsignaturas;
	ArrayList<Examen> examenes;
	ArrayList<Desempenio> desempenios;
	ArrayList<Inscripcion> inscripciones;
	ArrayList<RolDocente> roles;
	ArrayList<PlanDeEstudio> planes;
	ArrayList<Acta> actas;
	Acta actaElegida;

	private JTable tablaDocentes;
	private JTable tablaAlumnos;
	private JPanel jp;
	private JButton volver;
	private JButton imprimir;

	/**
	 * Constructor por parametros
	 *
	 * @param alumnos lista de alumnos 
	 * @param carreras lista de carreras
	 * @param docentes lista de docentes
	 * @param bitacoras lista de bitacoras
	 * @param asignaturas lista de asignaturas
	 * @param cargosAsignaturas lista de cargos de los docentes en las asignaturas
	 * @param examenes lista de examenes
	 * @param desempenios lista de desempenios
	 * @param inscripciones lista de inscripciones
	 * @param roles lista de roles de docentes
	 * @param planes lista de planes de estudio
	 * @param actas lista de actas
	 * @param tablaDocentes tabla de docentes
	 * @param tablaAlumnos tabla de alumnos
	 * @param actaElegida acta seleccionada
	 */
	PrintPrev(ArrayList<Alumno> alumnos, ArrayList<Carrera> carreras, ArrayList<Docente> docentes, ArrayList<Bitacora> bitacoras, ArrayList<Asignatura> asignaturas, ArrayList<CargoAsignatura> cargosAsignaturas, ArrayList<Examen> examenes, ArrayList<Desempenio> desempenios, ArrayList<Inscripcion> inscripciones, ArrayList<RolDocente> roles, ArrayList<PlanDeEstudio> planes, ArrayList<Acta> actas, JTable tablaDocentes, JTable tablaAlumnos, Acta actaElegida) {
		this.alumnos = alumnos;
		this.carreras = carreras;
		this.docentes = docentes;
		this.bitacoras = bitacoras;
		this.asignaturas = asignaturas;
		this.cargosAsignaturas = cargosAsignaturas;
		this.examenes = examenes;
		this.desempenios = desempenios;
		this.inscripciones = inscripciones;
		this.roles = roles;
		this.planes = planes;
		this.actas = actas;
		this.tablaAlumnos = tablaAlumnos;
		this.tablaDocentes = tablaDocentes;
		this.actaElegida = actaElegida;
		initComponents();
	}

		/**
	 * Setea los componentes de la ventana
	 */
	private void initComponents() {

		this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(800, 600));
		
		String[] fDocentes = {"Legajo", "Nombre", "Apellido", "Cargo"};
		String[] fAlumnos = {"Apellido", "Nombre", "Dni", "Regularidad", "nota"};

		JTable docentesImprimir = armarJTable(this.tablaDocentes,fDocentes);
		JTable alumnosImprimir = armarJTable(this.tablaAlumnos,fAlumnos);

		jp = new JPanel();

		volver = new JButton("Volver");

		volver.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				volverActionPerformed(evt);
			}
		});

		imprimir = new JButton("Imprimir");
		imprimir.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimirActionPerformed(evt);
			}
		});

		this.getContentPane().add(jp);

		jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
		jp.add(new JLabel((actaElegida.getExamenFinal().getMateria().getNombre())
			+ "    " + actaElegida.getExamenFinal().getFecha() + "   " + "Cerrada"));

		jp.add(Box.createVerticalStrut(20));
		jp.add(new JScrollPane(docentesImprimir));
		jp.add(Box.createVerticalStrut(10));
		jp.add(new JScrollPane(alumnosImprimir));
		jp.add(Box.createVerticalStrut(50));
		jp.add(new JLabel("Titular"
			+ "                                                         Vocal 1"
			+ "                                                         Vocal 2"));
		jp.add(Box.createHorizontalStrut(100));

		jp.add(volver);
		jp.add(Box.createVerticalStrut(10));
		jp.add(imprimir);
		pack();
	}
	
	
	/**
	 * Accion para el Boton Volver
	 *
	 * @param evt
	 */
	private void volverActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		//new ImprimirActa(alumnos, carreras, docentes, bitacoras, asignaturas, cargosAsignaturas, examenes, desempenios, inscripciones, roles, planes, actas).setVisible(true);
	}

	/**
	 * Accion para el boton imprimir
	 *
	 * @param evt
	 */
	private void imprimirActionPerformed(java.awt.event.ActionEvent evt) {
		this.actaElegida.setEstado(false);
		this.actaElegida.getExamenFinal().setAbierto(false);
		guardarNotasFinales();
		imprimir(this.jp);
	}

	/**
	 * Imprime todos los componentes de la ventana
	 *
	 * @param component
	 */
	public void imprimir(JPanel component) {
		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setJobName(" Print Component ");

		pj.setPrintable(new Printable() {
			public int print(Graphics pg, PageFormat pf, int pageNum) {
				if (pageNum > 0) {
					return Printable.NO_SUCH_PAGE;
				}

				Graphics2D g2 = (Graphics2D) pg;
				g2.translate(pf.getImageableX(), pf.getImageableY());
				component.paint(g2);
				return Printable.PAGE_EXISTS;
			}
		});
		if (pj.printDialog() == false) {
			return;
		}
		try {
			pj.print();
		} catch (PrinterException ex) {
			// handle exception
		}
	}



	/**
	 * Crea una nueva tabla a partir de las tablas pasadas por parametros y el
	 * array de String para los headers
	 * @param tabla tabla para usar
	 * @param filas headers
	 * @return tabla
	 */
	private JTable armarJTable(JTable tabla, String[] filas) {
		TableModelIneditable tablasIneditables = new TableModelIneditable(filas, tabla.getRowCount());
		
		JTable tablaReturn = new JTable(tablasIneditables);
		
		for (int i = 0; i < tabla.getRowCount(); i++) {
			for (int j = 0; j < tabla.getColumnCount(); j++) {
				tablaReturn.setValueAt(tabla.getModel().getValueAt(i, j), i, j);
			}
		}
		return tablaReturn;
		
	}

	
	/**
	 * Guarda en los desempenios del examen las notas finales, por si hubo que
	 * modificar algo
	 */
	private void guardarNotasFinales() {
		for (Desempenio desempenio : desempenios) {
			if (desempenio.getExamen().equals(actaElegida.getExamenFinal())) {
				for (int i = 0; i < this.tablaAlumnos.getRowCount(); i++) {
					if (this.tablaAlumnos.getModel().getValueAt(i, 2).equals(desempenio.getAlumno().getDni())) {
						int nota = obtenerNota((String) tablaAlumnos.getModel().getValueAt(i, 4));
						desempenio.setNota(nota);
					}
				}
				
			}
		}
	}

	/**
	 * Pasa la nota mostrada en la tabla a un integer, si la nota 
	 * contiene la leyenda "no se entrego", entonces la nota pasa a ser 0
	 * @param notaString
	 * @return String nota
	 */
	private int obtenerNota(String notaString) {
		int nota=0;
		if (!notaString.equals("no se presentó")) {
			nota = Integer.parseInt(notaString);
		}
		
		return nota;
	}
}
